package com.google.android.gcm.demo.server;

/**
 * User: tim
 * Date: 11/25/14
 */
public class Device {


    private String regId;
    private String email;
    private String deviceType;

    public Device() {

    }

    public Device(String regId, String email) {
        this.regId = regId;
        this.email = email;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
