package be.timvanginderen.waldofart;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Patterns;

import java.util.regex.Pattern;


public class FartHelper {

    private static final String PREFS = "be.timvanginderen.waldofart.prefs";
    private static final String PREF_KEY_HIDDEN = "hidden";

    public static void unhideApp(Context context) {
        PackageManager p = context.getPackageManager();
        ComponentName componentName = new ComponentName("be.timvanginderen.waldofart", "be.timvanginderen.waldofart.MainActivity");
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);

        SharedPreferences prefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(PREF_KEY_HIDDEN, false).commit();
    }
    public static void hideApp(Context context) {
        //Set app to uninstalled in app menu
        PackageManager p = context.getPackageManager();
        ComponentName componentName = new ComponentName("be.timvanginderen.waldofart", "be.timvanginderen.waldofart.MainActivity");
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

        SharedPreferences prefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(PREF_KEY_HIDDEN, true).commit();
    }
    public static boolean isAppHidden(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return prefs.getBoolean(PREF_KEY_HIDDEN, true);
    }

    public static String getEmail(Context context) {
        String email = "";
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                email = account.name;
            }
        }
        return email;
    }
}
