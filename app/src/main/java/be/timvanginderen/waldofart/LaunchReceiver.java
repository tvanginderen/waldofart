package be.timvanginderen.waldofart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class LaunchReceiver extends BroadcastReceiver {

//    public static final String EASTER_EGG = "###925363278###";
    public static final String EASTER_EGG = "###42###";

    @Override
    public void onReceive(Context context, Intent intent) {
        String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        if(EASTER_EGG.equals(number)) {
            Intent myIntent = new Intent(context,DemoActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);
//            abortBroadcast();
        }
    }

}