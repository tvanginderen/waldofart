package be.timvanginderen.waldofart;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

public class KeepAliveService extends Service {

    public static final String TAG = "GCM Demo";

    Handler mHandler;



    @Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();

        Log.i(TAG, "-------------------- start service");

//		try {
//			Log.i(TAG, "--------------------start service");
//			return;
//		} catch (ProvisioningException e) {
//			this.stopSelf();
//		}
//		Log.i(TAG, "--------------------failed start service");

        mHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoundMeter meter = new SoundMeter();
                try {
                    meter.start();
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                }
                while (true) {
                    try {
                        final double amplitude = meter.getAmplitude();

                        Thread.sleep(1000);
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                // Write your code here to update the UI.
                                Toast.makeText(getApplicationContext(), "Ampl: " + amplitude, Toast.LENGTH_LONG).show();
                            }
                        });



                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }).start();
    }
	
	@Override
	public void onDestroy() {
		Log.i(TAG, "-------------------- stop service");
		super.onDestroy();
	}
	
}
